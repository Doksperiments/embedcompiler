from string import Formatter


class EmbedFormatter(Formatter):

    def __init__(self):
        Formatter.__init__(self)

    def get_value(self, key, args, kwargs):
        if isinstance(key, str):
            try:
                return kwargs[key]
            except KeyError:
                return '{' + key + '}'
