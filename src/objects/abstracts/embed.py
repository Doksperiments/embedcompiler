from abc import ABC, abstractmethod


class EmbedAbstract(ABC):

    class MediaAbstract(ABC):

        @abstractmethod
        def __init__(self, url: str, width: int, height: int):

            self.url = url
            self.width = width
            self.height = height

    class ImageAbstract(ABC):

        @abstractmethod
        def __init__(self, url: str, width: int, height: int):
            raise NotImplementedError

    class ThumbnailAbstract(ABC):

        @abstractmethod
        def __init__(self, url: str, width: int, height: int):
            raise NotImplementedError

    class AuthorAbstract(ABC):

        @abstractmethod
        def __init__(self, name: str, url: str, icon_url: str):

            self.name = name
            self.url = url
            self.icon_url = icon_url

    class FooterAbstract(ABC):

        @abstractmethod
        def __init__(self, text: str, icon_url: str):

            self.text = text
            self.icon_url = icon_url

    class FieldAbstract(ABC):

        @abstractmethod
        def __init__(self, name: str, value: str, inline: bool = False):
            self.name = name
            self.value = value
            self.inline = inline

    class VideoAbstract(ABC):

        @abstractmethod
        def __init__(self, url: str, width: int, height: int):
            raise NotImplementedError

    @abstractmethod
    def __init__(self):

        self.color: int = 0
        self.description: str = ""
        self.timestamp: str = ""
        self.title: str = ""
        self.url: str = ""

        self.author: EmbedAbstract.AuthorAbstract = None
        self.fields: list = []
        self.footer: EmbedAbstract.FooterAbstract = None
        self.image: EmbedAbstract.ImageAbstract = None
        self.thumbnail: EmbedAbstract.ThumbnailAbstract = None
        self.video: EmbedAbstract.VideoAbstract = None
