from .abstracts.embed import EmbedAbstract
from datetime import datetime


class Embed(EmbedAbstract):

    class Media(EmbedAbstract.MediaAbstract):

        def __init__(self, url: str, width: int, height: int):

            self.url = url
            self.width = width
            self.height = height

    class Image(Media, EmbedAbstract.ImageAbstract):

        def __init__(self, url: str, width: int, height: int):
            super(Embed.Media, self).__init__(url, width, height)

    class Thumbnail(Media, EmbedAbstract.ThumbnailAbstract):

        def __init__(self, url: str, width: int, height: int):
            super(Embed.Media, self).__init__(url, width, height)

    class Author(EmbedAbstract.AuthorAbstract):

        def __init__(self, name: str, url: str, icon_url: str):

            self.name = name
            self.url = url
            self.icon_url = icon_url

    class Footer(EmbedAbstract.FooterAbstract):

        def __init__(self, text: str, icon_url: str):

            self.text = text
            self.icon_url = icon_url

    class Field(EmbedAbstract.FieldAbstract):

        def __init__(self, name: str, value: str, inline: bool = False):
            self.name = name
            self.value = value
            self.inline = inline

    class Video(Media, EmbedAbstract.VideoAbstract):

        def __init__(self, url: str, width: int, height: int):
            super(Embed.Media, self).__init__(url, width, height)

    def __init__(self):

        self.color: int = 0
        self.description: str = ""
        self.timestamp: datetime = datetime.utcnow()
        self.title: str = ""
        self.url: str = ""

        self.author: Embed.Author = None
        self.fields: list = []
        self.footer: Embed.Footer = None
        self.image: Embed.Image = None
        self.thumbnail: Embed.Thumbnail = None
        self.video: Embed.Video = None

    def in_length_limit(self) -> bool:
        total_chars = 0

        total_chars += len(self.title) if self.title else 0
        total_chars += len(self.description) if self.description else 0
        total_chars += len(self.footer.text) if self.footer else 0
        total_chars += len(self.author.name) if self.author else 0

        for field in self.fields:
            for string in [field.name, field.value]:
                total_chars += len(string)

        if total_chars > 6000:
            return False
        return True
