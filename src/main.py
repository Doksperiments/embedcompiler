from .objects.embed import Embed
from .objects.formatter import EmbedFormatter

from datetime import datetime


class EmbedCompiler:

    def __init__(self):
        self.embed = Embed()

        self.__formatter = EmbedFormatter()

    def set_title(self, title, fmt_str=None):

        if len(title) > 256:
            raise ValueError("Title too long (max 256)")
        else:
            self.embed.title = self.__formatter.format(title, **fmt_str) if fmt_str else title

    def set_description(self, description, fmt_str=None):

        if len(description) > 2048:
            raise ValueError("Description too long (max 2048)")
        else:
            self.embed.description = self.__formatter.format(description, **fmt_str) if fmt_str else description

    def set_color(self, color):

        if type(color) != int:
            raise ValueError("Color not in the right format (integer)")
        else:
            self.embed.color = color

    def set_timestamp(self, **timestamp):

        self.embed.timestamp = datetime(
            year=timestamp.get('year', 0),
            month=timestamp.get('month', 0),
            day=timestamp.get('day', 0),
            hour=timestamp.get('hour', 0),
            minute=timestamp.get('minute', 0),
            second=timestamp.get('second', 0)
        )

    def set_url(self, url):
        self.embed.url = url

    def set_author(self, name, url=None, icon_url=None, fmt_str=None):

        if len(name) > 256:
            raise ValueError("Author name too long")
        else:
            name = self.__formatter.format(name, **fmt_str) if fmt_str else name
            self.embed.author = Embed.Author(name, url, icon_url)

    def add_field(self, name, value, inline=False, fmt_str=None):

        if len(self.embed.fields) > 25:
            raise ValueError("Max fields limit reached (25)")

        if len(name) > 256:
            raise ValueError("Field name too long (max 256)")
        elif len(value) > 1024:
            raise ValueError("Value too long (max 1024)")
        else:
            name = self.__formatter.format(name, **fmt_str) if fmt_str else name
            value = self.__formatter.format(value, **fmt_str) if fmt_str else value
            self.embed.fields.append(Embed.Field(name, value, inline))

    def set_footer(self, text, icon_url=None, fmt_str=None):

        if len(text) > 2048:
            raise ValueError("Footer text too long (max 2048)")
        else:
            text = self.__formatter.format(text, **fmt_str) if fmt_str else text
            self.embed.footer = Embed.Footer(text, icon_url)

    def set_image(self, url, width=None, height=None):
        self.embed.image = Embed.Image(url, width, height)

    def set_thumbnail(self, url, width=None, height=None):
        self.embed.thumbnail = Embed.Thumbnail(url, width, height)

    def set_video(self, url, width=None, height=None):
        self.embed.video = Embed.Video(url, width, height)

    def to_dict(self):

        if self.embed.in_length_limit():

            embed_dict = self.embed
            embed_dict.timestamp = self.embed.timestamp.strftime("%Y-%m-%d %H:%M:%S") if self.embed.timestamp else None
            embed_dict.author = self.embed.author.__dict__ if self.embed.author else None

            fields = []
            for field in embed_dict.fields:
                fields.append(field.__dict__ if field else None)

            embed_dict.fields.clear()
            embed_dict.fields = fields if fields else None

            embed_dict.footer = self.embed.footer.__dict__ if self.embed.footer else None
            embed_dict.image = self.embed.image.__dict__ if self.embed.image else None
            embed_dict.thumbnail = self.embed.thumbnail.__dict__ if self.embed.thumbnail else None
            embed_dict.video = self.embed.video.__dict__ if self.embed.video else None

            return embed_dict.__dict__

        else:
            raise ValueError("The max embed text limit shouldn't exceed 6000 characters")

    def from_dict(self, embed_dict, fmt_str):

        for item in ['title', 'description', 'color', 'url']:
            try:
                # Change to safer method
                if item == 'color' or item == 'url':
                    eval(f"self.set_{item}(embed_dict['{item}'])")
                else:
                    eval(f"self.set_{item}(embed_dict['{item}'], fmt_str={fmt_str})")
            except KeyError:
                pass

        for item in ['author', 'footer', 'image', 'thumbnail', 'video', 'timestamp']:
            try:
                # Change to safer method
                if item == 'author' or item == 'footer':
                    embed_dict[item]['fmt_str'] = fmt_str

                eval(f"self.set_{item}(**embed_dict['{item}'])")
            except KeyError:
                pass

        try:
            for field in embed_dict["fields"]:
                field['fmt_str'] = fmt_str
                self.add_field(**field)
        except KeyError:
            pass

        return self
